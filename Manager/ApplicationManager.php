<?php
/**
 * \file ApplicationManager.php
 * \author Pierre TRANCHARD <pierre@tranchard.net>
 * \version 1.0
 * \date 20/06/15
 * \brief
 * \details
 */

namespace Spark\RepositoryMonitorBundle\Manager;

use Doctrine\ORM\EntityManager;
use Spark\RepositoryMonitorBundle\Entity\Application;
use Spark\RepositoryMonitorBundle\Repository\ApplicationRepository;

/**
 * Class ApplicationManager
 *
 * @package Spark\RepositoryMonitorBundle\Manager
 */
class ApplicationManager extends BaseManager
{

    /**
     * @var ApplicationRepository
     */
    protected $repository;

    /**
     * Constructor
     *
     * @param EntityManager         $entityManager
     * @param ApplicationRepository $repository
     */
    public function __construct(EntityManager $entityManager, ApplicationRepository $repository)
    {
        parent::__construct($entityManager);
        $this->repository = $repository;
    }

    /**
     * @return Application
     */
    public function createInstance()
    {
        return new Application();
    }

    /**
     * @param Application $application
     *
     * @return bool
     * @throws \Exception
     */
    public function deleteInstance(Application $application)
    {
        try {
            $this->entityManager->remove($application);

            $this->entityManager->flush();

            return true;
        } catch (\Exception $exception) {
            throw $exception;
        }
    }

    /**
     * @param Application $application
     *
     * @return bool
     * @throws \Exception
     */
    public function persistInstance(Application $application)
    {
        try {
            $this->entityManager->persist($application);

            return true;
        } catch (\Exception $exception) {
            throw $exception;
        }
    }

    /**
     * @return ApplicationRepository
     */
    public function getRepository()
    {
        return $this->repository;
    }
}
