<?php
/**
 * \file BaseManager.php
 * \author Pierre TRANCHARD <pierre@tranchard.net>
 * \version 1.0
 * \date 20/06/15
 * \brief
 * \details
 */

namespace Spark\RepositoryMonitorBundle\Manager;

use Doctrine\ORM\EntityManager;

/**
 * Class BaseManager
 *
 * @package Spark\RepositoryMonitorBundle\Manager
 */
abstract class BaseManager implements ManagerInterface
{

    /**
     * @var EntityManager
     */
    protected $entityManager;

    /**
     * Constructor
     *
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @return EntityManager
     */
    public function getEntityManager()
    {
        return $this->entityManager;
    }
}