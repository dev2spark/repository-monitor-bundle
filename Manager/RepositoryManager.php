<?php
/**
 * \file RepositoryManager.php
 * \author Pierre TRANCHARD <pierre@tranchard.net>
 * \version 1.0
 * \date 13/07/15
 * \brief
 * \details
 */

namespace Spark\RepositoryMonitorBundle\Manager;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Spark\RepositoryMonitorBundle\Entity\Application;
use Spark\RepositoryMonitorBundle\Entity\Repository;

/**
 * Class RepositoryManager
 *
 * @package Spark\RepositoryMonitorBundle\Manager
 */
class RepositoryManager extends BaseManager
{

    /**
     * @var EntityRepository
     */
    protected $repository;

    /**
     * Constructor
     *
     * @param EntityManager    $entityManager
     * @param EntityRepository $repository
     */
    public function __construct(EntityManager $entityManager, EntityRepository $repository)
    {
        parent::__construct($entityManager);
        $this->repository = $repository;
    }

    /**
     * @return Repository
     */
    public function createInstance()
    {
        return new Repository();
    }

    /**
     * @param Repository $repository
     *
     * @return bool
     * @throws \Exception
     */
    public function deleteInstance(Repository $repository)
    {
        try {
            foreach ($repository->getApplications() as $application) {
                /** @var $application Application */
                $application->setRepository(null);
            }

            $this->entityManager->remove($repository);

            $this->entityManager->flush();

            return true;
        } catch (\Exception $exception) {
            throw $exception;
        }
    }

    /**
     * @param Repository $repository
     *
     * @return bool
     * @throws \Exception
     */
    public function persistInstance(Repository $repository)
    {
        try {
            $this->entityManager->persist($repository);

            return true;
        } catch (\Exception $exception) {
            throw $exception;
        }
    }

    /**
     * @return EntityRepository
     */
    public function getRepository()
    {
        return $this->repository;
    }
}
