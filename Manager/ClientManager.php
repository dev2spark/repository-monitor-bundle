<?php
/**
 * \file ClientManager.php
 * \author Pierre TRANCHARD <pierre@tranchard.net>
 * \version 1.0
 * \date 26/06/15
 * \brief
 * \details
 */

namespace Spark\RepositoryMonitorBundle\Manager;

use Doctrine\ORM\EntityManager;
use GuzzleHttp\Client as GuzzleClient;
use Spark\RepositoryMonitorBundle\Entity\Application;
use Spark\RepositoryMonitorBundle\Entity\Client;
use Spark\RepositoryMonitorBundle\Repository\ClientRepository;

/**
 * Class ClientManager
 *
 * @package Spark\RepositoryMonitorBundle\Manager
 */
class ClientManager extends BaseManager
{

    /**
     * @var ClientRepository
     */
    protected $repository;

    /**
     * @var VersionManager
     */
    protected $versionManager;

    /**
     * @var GuzzleClient
     */
    protected $guzzle;

    /**
     * Constructor
     *
     * @param EntityManager    $entityManager
     * @param ClientRepository $repository
     * @param VersionManager   $versionManager
     */
    public function __construct(
        EntityManager $entityManager,
        ClientRepository $repository,
        VersionManager $versionManager
    ) {
        parent::__construct($entityManager);
        $this->repository     = $repository;
        $this->versionManager = $versionManager;
        $this->guzzle         = new GuzzleClient(
            array(
                'timeout' => 5,
            )
        );
    }

    /**
     * @return Client
     */
    public function createInstance()
    {
        return new Client();
    }

    /**
     * @param Client $client
     *
     * @return bool
     * @throws \Exception
     */
    public function deleteInstance(Client $client)
    {
        try {
            $this->entityManager->remove($client);

            $this->entityManager->flush();

            return true;
        } catch (\Exception $exception) {
            throw $exception;
        }
    }

    /**
     * @param Client $client
     *
     * @return bool
     * @throws \Exception
     */
    public function persistInstance(Client $client)
    {
        try {
            $this->entityManager->persist($client);

            return true;
        } catch (\Exception $exception) {
            throw $exception;
        }
    }

    /**
     * @param array $errors
     */
    public function refreshVersion(array &$errors = array())
    {
        $clients = $this->getRepository()->findAll();

        foreach ($clients as $client) {
            /** @var $client Client */
            $applications = $client->getApplications();
            foreach ($applications as $application) {
                /** @var $application Application */
                $lastVersion = $application->getVersions()->count() > 0 ? $application->getVersions()->last() : null;
                try {
                    $onlineVersion = $this->guzzle->get(sprintf("%s/version", $application->getUri()));

                    $version = $this->versionManager->createInstance(
                        json_decode($onlineVersion->getBody()->getContents(), true)
                    );

                    if (is_null($lastVersion) === false) {
                        if ($this->versionManager->isOutDated($lastVersion, $version)) {
                            $version->setApplication($application);
                            $this->versionManager->persistInstance($version);
                            $this->entityManager->flush();
                        }
                    } else {
                        $version->setApplication($application);
                        $this->versionManager->persistInstance($version);
                        $this->entityManager->flush();
                    }
                } catch (\Exception $e) {
                    $errors[] =
                        sprintf(
                            'Command failed for this reason: %s for the application %s',
                            $e->getMessage(),
                            $application->getName()
                        );

                    continue;
                }
            }
        }
    }

    /**
     * @return ClientRepository
     */
    public function getRepository()
    {
        return $this->repository;
    }

    /**
     * @return array
     */
    public function getPlatforms()
    {
        $clients   = $this->getRepository()->findAll();
        $platforms = array();

        foreach ($clients as $client) {
            /** @var $client Client */
            $applications = array();
            if ($client->getApplications()->count() > 0) {
                foreach ($client->getApplications() as $application) {
                    /** @var $application Application */
                    $applications[] = array(
                        'name'    => $application->getName(),
                        'slug'    => $application->getSlug(),
                        'uri'     => $application->getUri(),
                        'version' => $application->getVersions()->last()
                    );
                }
            }

            $platforms[$client->getCode()] = array('name'         => $client->getName(),
                                                   'code'         => $client->getCode(),
                                                   'applications' => $applications
            );
        }

        return $platforms;
    }
}
