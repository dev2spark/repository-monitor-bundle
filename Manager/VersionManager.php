<?php
/**
 * \file VersionManager.php
 * \author Pierre TRANCHARD <pierre@tranchard.net>
 * \version 1.0
 * \date 26/06/15
 * \brief
 * \details
 */

namespace Spark\RepositoryMonitorBundle\Manager;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Spark\RepositoryMonitorBundle\Entity\Version;

/**
 * Class VersionManager
 *
 * @package Spark\RepositoryMonitorBundle\Manager
 */
class VersionManager extends BaseManager
{

    /**
     * @var EntityRepository
     */
    protected $repository;

    /**
     * Constructor
     *
     * @param EntityManager    $entityManager
     * @param EntityRepository $repository
     */
    public function __construct(EntityManager $entityManager, EntityRepository $repository)
    {
        parent::__construct($entityManager);
        $this->repository = $repository;
    }

    /**
     * @return EntityRepository
     */
    public function getRepository()
    {
        return $this->repository;
    }

    /**
     * @param array $onlineVersion
     *
     * @return Version
     */
    public function createInstance(array $onlineVersion = array())
    {
        $version = new Version();
        if (empty($onlineVersion) === false) {
            $version->setBuild($onlineVersion['build']);
            $version->setRevision($onlineVersion['revision']);
            $version->setDate(\DateTime::createFromFormat("Y/m/d-H:i", $onlineVersion['date']));
        }

        return $version;
    }

    /**
     * @param Version $version
     *
     * @return bool
     * @throws \Exception
     */
    public function deleteInstance(Version $version)
    {
        try {
            $version->getApplication()->removeVersion($version);

            $this->entityManager->remove($version);

            $this->entityManager->flush();

            return true;
        } catch (\Exception $exception) {
            throw $exception;
        }
    }

    /**
     * @param Version $version
     *
     * @return bool
     * @throws \Exception
     */
    public function persistInstance(Version $version)
    {
        try {
            $this->entityManager->persist($version);

            return true;
        } catch (\Exception $exception) {
            throw $exception;
        }
    }

    /**
     * @param Version $version
     * @param Version $onlineVersion
     *
     * @return bool
     */
    public function isOutDated(Version $version, Version $onlineVersion)
    {
        if ($version->getDate()->getTimestamp() < $version->getDate()->getTimestamp()) {
            return true;
        }

        return false;
    }
}
