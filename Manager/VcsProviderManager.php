<?php
/**
 * \file VcsProviderManager.php
 * \author Pierre TRANCHARD <pierre@tranchard.net>
 * \version 1.0
 * \date 13/07/15
 * \brief
 * \details
 */

namespace Spark\RepositoryMonitorBundle\Manager;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Spark\RepositoryMonitorBundle\Entity\Application;
use Spark\RepositoryMonitorBundle\Entity\Repository;
use Spark\RepositoryMonitorBundle\Entity\VcsProvider;

/**
 * Class VcsProviderManager
 *
 * @package Spark\RepositoryMonitorBundle\Manager
 */
class VcsProviderManager extends BaseManager
{

    /**
     * @var EntityRepository
     */
    protected $repository;

    /**
     * Constructor
     *
     * @param EntityManager    $entityManager
     * @param EntityRepository $repository
     */
    public function __construct(EntityManager $entityManager, EntityRepository $repository)
    {
        parent::__construct($entityManager);
        $this->repository = $repository;
    }

    /**
     * @return VcsProvider
     */
    public function createInstance()
    {
        return new VcsProvider();
    }

    /**
     * @param VcsProvider $vcsProvider
     *
     * @return bool
     * @throws \Exception
     */
    public function deleteInstance(VcsProvider $vcsProvider)
    {
        try {
            if ($vcsProvider->getRepositories()->count() > 0) {
                foreach ($vcsProvider->getRepositories() as $repository) {
                    /** @var $repository Repository */
                    if ($repository->getApplications()->count() > 0) {
                        foreach ($repository->getApplications() as $application) {
                            /** @var $application Application */
                            $this->entityManager->remove($application);
                        }
                    }
                }
            }

            $this->entityManager->remove($vcsProvider);

            $this->entityManager->flush();

            return true;
        } catch (\Exception $exception) {
            throw $exception;
        }
    }

    /**
     * @param VcsProvider $vcsProvider
     *
     * @return bool
     * @throws \Exception
     */
    public function persistInstance(VcsProvider $vcsProvider)
    {
        try {
            $this->entityManager->persist($vcsProvider);

            return true;
        } catch (\Exception $exception) {
            throw $exception;
        }
    }

    /**
     * @return EntityRepository
     */
    public function getRepository()
    {
        return $this->repository;
    }
}
