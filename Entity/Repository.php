<?php
/**
 * \file Repository.php
 * \author Pierre TRANCHARD <pierre@tranchard.net>
 * \version 1.0
 * \date 20/06/15
 * \brief
 * \details
 */

namespace Spark\RepositoryMonitorBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Repository
 *
 * @package Spark\RepositoryMonitorBundle\Entity
 *
 * @ORM\Entity
 * @ORM\Table("repository")
 */
class Repository
{

    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=100)
     */
    protected $uri;

    /**
     * @var VcsProvider
     *
     * @ORM\ManyToOne(targetEntity="Spark\RepositoryMonitorBundle\Entity\VcsProvider", inversedBy="repositories")
     * @ORM\JoinColumns({
     * @ORM\JoinColumn(name="vcs_provider_id", referencedColumnName="id")
     * })
     */
    protected $vcsProvider;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="Spark\RepositoryMonitorBundle\Entity\Application", mappedBy="repository",
     *                                                                                 cascade="all")
     */
    protected $applications;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->applications = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getUri()
    {
        return $this->uri;
    }

    /**
     * @param string $uri
     *
     * @return $this
     */
    public function setUri($uri)
    {
        $this->uri = $uri;

        return $this;
    }

    /**
     * @return VcsProvider
     */
    public function getVcsProvider()
    {
        return $this->vcsProvider;
    }

    /**
     * @param null|VcsProvider $vcsProvider
     *
     * @return $this
     */
    public function setVcsProvider(VcsProvider $vcsProvider = null)
    {
        $this->vcsProvider = $vcsProvider;
        if (is_null($vcsProvider) === false && $vcsProvider->hasRepository($this) === false) {
            $vcsProvider->addRepository($this);
        }

        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getApplications()
    {
        return $this->applications;
    }

    /**
     * @param ArrayCollection $applications
     *
     * @return $this
     */
    public function setApplications($applications)
    {
        $this->applications = $applications;

        return $this;
    }

    /**
     * @param Application $application
     *
     * @return bool
     */
    public function hasApplication(Application $application)
    {
        return $this->applications->contains($application);
    }

    /**
     * @param Application $application
     *
     * @return bool
     */
    public function addApplication(Application $application)
    {
        if ($this->hasApplication($application) === false) {
            if (is_null($application->getRepository())) {
                $application->setRepository($this);
            }

            return $this->applications->add($application);
        }

        return false;
    }

    /**
     * @param Application $application
     *
     * @return bool
     */
    public function removeApplication(Application $application)
    {
        if ($this->hasApplication($application)) {
            $application->setRepository(null);

            $this->applications->removeElement($application);
        }

        return false;
    }
}
