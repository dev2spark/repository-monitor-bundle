<?php
/**
 * \file Application.php
 * \author Pierre TRANCHARD <pierre@tranchard.net>
 * \version 1.0
 * \date 20/06/15
 * \brief
 * \details
 */

namespace Spark\RepositoryMonitorBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Class Application
 *
 * @package Spark\RepositoryMonitorBundle\Entity
 *
 * @ORM\Entity(repositoryClass="Spark\RepositoryMonitorBundle\Repository\ApplicationRepository")
 * @ORM\Table("application")
 */
class Application
{

    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @Gedmo\Slug(fields={"name"}, updatable=false)
     * @ORM\Column(type="string", length=100, unique=true)
     */
    protected $slug;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=100)
     */
    protected $name;

    /**
     * @var string
     *
     * @ORM\Column(name="directory", type="string", length=255, nullable=true)
     */
    protected $directory;

    /**
     * @var string|null
     *
     * @ORM\Column(name="uri", type="string", length=255, nullable=true)
     */
    protected $uri;

    /**
     * @var Repository
     *
     * @ORM\ManyToOne(targetEntity="Spark\RepositoryMonitorBundle\Entity\Repository", inversedBy="applications")
     * @ORM\JoinColumns({
     * @ORM\JoinColumn(name="application_id", referencedColumnName="id")
     * })
     */
    protected $repository;

    /**
     * @var Client
     *
     * @ORM\ManyToOne(targetEntity="Spark\RepositoryMonitorBundle\Entity\Client", inversedBy="applications")
     * @ORM\JoinColumns({
     * @ORM\JoinColumn(name="client_id", referencedColumnName="id")
     * })
     */
    protected $client;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="Spark\RepositoryMonitorBundle\Entity\Version", mappedBy="application",
     *                                                                             cascade="all")
     */
    protected $versions;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->versions = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @param string $slug
     *
     * @return $this
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getDirectory()
    {
        return $this->directory;
    }

    /**
     * @param string $directory
     *
     * @return $this
     */
    public function setDirectory($directory)
    {
        $this->directory = $directory;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getUri()
    {
        return $this->uri;
    }

    /**
     * @param string|null $uri
     *
     * @return $this
     */
    public function setUri($uri = null)
    {
        $this->uri = $uri;

        return $this;
    }

    /**
     * @return Repository
     */
    public function getRepository()
    {
        return $this->repository;
    }

    /**
     * @param null|Repository $repository
     *
     * @return $this
     */
    public function setRepository(Repository $repository = null)
    {
        $this->repository = $repository;
        if (is_null($repository) === false && $repository->hasApplication($this) === false) {
            $repository->addApplication($this);
        }

        return $this;
    }

    /**
     * @return Client
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * @param null|Client $client
     *
     * @return $this
     */
    public function setClient(Client $client = null)
    {
        $this->client = $client;
        if (is_null($client) === false && $client->hasApplication($this) === false) {
            $client->addApplication($this);
        }

        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getVersions()
    {
        return $this->versions;
    }

    /**
     * @param ArrayCollection $versions
     *
     * @return $this
     */
    public function setVersions($versions)
    {
        $this->versions = $versions;

        return $this;
    }

    /**
     * @param Version $version
     *
     * @return bool
     */
    public function hasVersion(Version $version)
    {
        return $this->versions->contains($version);
    }

    /**
     * @param Version $version
     *
     * @return bool
     */
    public function addVersion(Version $version)
    {
        if ($this->hasVersion($version) === false) {
            if (is_null($version->getApplication())) {
                $version->setApplication($this);
            }

            return $this->versions->add($version);
        }

        return false;
    }

    /**
     * @param Version $version
     *
     * @return bool
     */
    public function removeVersion(Version $version)
    {
        if ($this->hasVersion($version)) {
            $version->setApplication(null);

            return $this->versions->removeElement($version);
        }

        return false;
    }
}
