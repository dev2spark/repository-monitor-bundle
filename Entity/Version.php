<?php
/**
 * \file Version.php
 * \author Pierre TRANCHARD <pierre@tranchard.net>
 * \version 1.0
 * \date 22/06/15
 * \brief
 * \details
 */

namespace Spark\RepositoryMonitorBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class Version
 *
 * @package Spark\RepositoryMonitorBundle\Entity
 *
 * @ORM\Entity
 * @ORM\Table(name="version")
 */
class Version
{

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="build", type="string", length=20, nullable=false)
     */
    protected $build;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime", nullable=false)
     */
    protected $date;

    /**
     * @var string
     *
     * @ORM\Column(name="revision", type="string", length=20, nullable=false)
     */
    protected $revision;

    /**
     * @var Application
     *
     * @ORM\ManyToOne(targetEntity="Spark\RepositoryMonitorBundle\Entity\Application", inversedBy="versions")
     * @ORM\JoinColumns({
     * @ORM\JoinColumn(name="application_id", referencedColumnName="id")
     * })
     */
    protected $application;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getBuild()
    {
        return $this->build;
    }

    /**
     * @param string $build
     *
     * @return $this
     */
    public function setBuild($build)
    {
        $this->build = $build;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param \DateTime $date
     *
     * @return $this
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * @return string
     */
    public function getRevision()
    {
        return $this->revision;
    }

    /**
     * @param string $revision
     *
     * @return $this
     */
    public function setRevision($revision)
    {
        $this->revision = $revision;

        return $this;
    }

    /**
     * @return Application
     */
    public function getApplication()
    {
        return $this->application;
    }

    /**
     * @param null|Application $application
     *
     * @return $this
     */
    public function setApplication(Application $application = null)
    {
        $this->application = $application;
        if (is_null($application) === false && $application->hasVersion($this) === false) {
            $application->addVersion($this);
        }

        return $this;
    }
}
