<?php
/**
 * \file Client.php
 * \author Pierre TRANCHARD <pierre@tranchard.net>
 * \version 1.0
 * \date 25/06/15
 * \brief
 * \details
 */

namespace Spark\RepositoryMonitorBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Client
 *
 * @package Spark\RepositoryMonitorBundle\Entity
 *
 * @ORM\Entity(repositoryClass="Spark\RepositoryMonitorBundle\Repository\ClientRepository")
 * @ORM\Table("client")
 */
class Client
{

    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=10)
     */
    protected $code;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=100)
     */
    protected $name;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="Spark\RepositoryMonitorBundle\Entity\Application", mappedBy="client",
     *                                                                                 cascade="all")
     */
    protected $applications;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->applications = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param string $code
     *
     * @return $this
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getApplications()
    {
        return $this->applications;
    }

    /**
     * @param ArrayCollection $applications
     *
     * @return $this
     */
    public function setApplications($applications)
    {
        $this->applications = $applications;

        return $this;
    }

    /**
     * @param Application $application
     *
     * @return bool
     */
    public function hasApplication(Application $application)
    {
        return $this->applications->contains($application);
    }

    /**
     * @param Application $application
     *
     * @return bool
     */
    public function addApplication(Application $application)
    {
        if ($this->hasApplication($application) === false) {
            if (is_null($application->getClient())) {
                $application->setClient($this);
            }

            return $this->applications->add($application);
        }

        return false;
    }

    /**
     * @param Application $application
     *
     * @return bool
     */
    public function removeApplication(Application $application)
    {
        if ($this->hasApplication($application)) {
            $application->setClient(null);

            $this->applications->removeElement($application);
        }

        return false;
    }
}
