<?php
/**
 * \file VcsProvider.php
 * \author Pierre TRANCHARD <pierre@tranchard.net>
 * \version 1.0
 * \date 20/06/15
 * \brief
 * \details
 */

namespace Spark\RepositoryMonitorBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class VcsProvider
 *
 * @package Spark\RepositoryMonitorBundle\Entity
 *
 * @ORM\Entity
 * @ORM\Table("vcs_provider")
 */
class VcsProvider
{

    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=100)
     */
    protected $name;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=100)
     */
    protected $uri;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="Spark\RepositoryMonitorBundle\Entity\Repository", mappedBy="vcsProvider",
     *                                                                                cascade="all")
     */
    protected $repositories;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->repositories = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getUri()
    {
        return $this->uri;
    }

    /**
     * @param string $uri
     *
     * @return $this
     */
    public function setUri($uri)
    {
        $this->uri = $uri;

        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getRepositories()
    {
        return $this->repositories;
    }

    /**
     * @param ArrayCollection $repositories
     *
     * @return $this
     */
    public function setRepositories($repositories)
    {
        $this->repositories = $repositories;

        return $this;
    }

    /**
     * @param Repository $repository
     *
     * @return bool
     */
    public function hasRepository(Repository $repository)
    {
        return $this->repositories->contains($repository);
    }

    /**
     * @param Repository $repository
     *
     * @return bool
     */
    public function addRepository(Repository $repository)
    {
        if ($this->hasRepository($repository) === false) {
            if (is_null($repository->getVcsProvider())) {
                $repository->setVcsProvider($this);
            }

            return $this->repositories->add($repository);
        }

        return false;
    }

    /**
     * @param Repository $repository
     *
     * @return bool
     */
    public function removeRepository(Repository $repository)
    {
        if ($this->hasRepository($repository)) {
            $repository->setVcsProvider(null);

            return $this->repositories->removeElement($repository);
        }

        return false;
    }
}
