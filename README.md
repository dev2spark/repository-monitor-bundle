Spark Repository Monitor Bundle
===========================


This bundle is designed to provide you a summary of your deployed applications and your standalone bundles.  

----------

Requirements
-----------------
> **This bundle relies on the following bundles and components:**

   > - Symfony 2.7.*
   > - Spark Framework Bundle
   > - OldSound RabbitMQ Bundle
   > - Guzzle HTTP Client
   > - Knp Paginator Bundle

It also needs for deployed application that a /version route is exposed with the following structure in JSON format.

```
#!javascript
{
	build: "0.6",
	date: "2015-08-05 17:38:04",
	deployment: "successful",
	environment: "production",
	revision: "02df2d4"
}
```

Bundle Configuration
--------------------

```
#!yml
# config.yml
spark_repository_monitor:
    ticket_manager_uri: #to be filled example : https://your-company.atlassian.net
    ticket_regex: "[a-zA-Z]+-\d+" #default value
    base_folders: ['Sites', 'www'] #default value
    default_shown_tags: 3 #default value
# routing.yml
spark_repository_monitor:
    resource: '@SparkRepositoryMonitorBundle/Resources/config/routing/routing.yml'
```


Enhancements
-----------------
The model is planed to use a ssh connection to perform the git command, but actually it relies on the project folders, and it infers that the clone is up to date with the repository.

Acknowledgements
-----------------------
I would like to thank two guys with who I worked since the very first version of the project. 

> Benoit Maziere: 

   > - GitHub: [bmaziere](https://github.com/bmaziere)
   > - Email: [benoit.maziere@gmail.com](mailto:benoit.maziere@gmail.com?subject=Repository%20Monitor)

> Abdoul N'Diaye: 

   > - GitHub: [Abdoul N'Diaye](https://github.com/AbdoulNdiaye)
   > - Email: [abdoul.nd@gmail.com](mailto:abdoul.nd@gmail.com?subject=Repository%20Monitor)
   > - Twitter: [@AbdoulNDiaye](https://twitter.com/AbdoulNDiaye)