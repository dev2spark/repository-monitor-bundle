<?php
/**
 * \file RefreshDeploymentCommand.php
 * \author Pierre TRANCHARD <pierre@tranchard.net>
 * \version 1.0
 * \date 10/07/15
 * \brief
 * \details
 */

namespace Spark\RepositoryMonitorBundle\Command;

use Doctrine\ORM\EntityManager;
use Spark\RepositoryMonitorBundle\Entity\Application;
use Spark\RepositoryMonitorBundle\Entity\Client;
use Spark\RepositoryMonitorBundle\Services\RefreshDeploymentProducer;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class RefreshDeploymentCommand
 *
 * @package Spark\RepositoryMonitorBundle\Command
 */
class RefreshDeploymentCommand extends ContainerAwareCommand
{

    /**
     * @var string|null
     */
    protected $client;

    /**
     * @var string|null
     */
    protected $application;

    /**
     * @var EntityManager
     */
    protected $entityManager;

    /**
     * @var RefreshDeploymentProducer
     */
    protected $producer;

    /**
     * @var OutputInterface
     */
    protected $output;

    /**
     * Configure the command
     */
    protected function configure()
    {
        $this->setName('spark:repository_monitor:refresh-deployment')
            ->setDescription('Refresh the database version of declared applications')
            ->addOption(
                'client',
                'c',
                InputOption::VALUE_OPTIONAL,
                'if you want to refresh the versions for all the client\'s applications (client code is expected)',
                null
            )
            ->addOption(
                'application',
                'app',
                InputOption::VALUE_OPTIONAL,
                'If you want to refresh the version of the given application slug',
                null
            );
    }

    /**
     * Initialize the command
     *
     * @param InputInterface  $input
     * @param OutputInterface $output
     */
    public function initialize(InputInterface $input, OutputInterface $output)
    {
        $container           = $this->getContainer();
        $this->client        = $input->getOption('client');
        $this->application   = $input->getOption('application');
        $this->entityManager = $container->get('doctrine.orm.entity_manager');
        $this->producer      = $container->get('spark_repository_monitor.services.refresh_deployment_producer');
        $this->output        = $output;
    }

    /**
     * Execute the command
     *
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return int|null|void
     */
    public function execute(InputInterface $input, OutputInterface $output)
    {
        $clientRepository      = $this->entityManager->getRepository('SparkRepositoryMonitorBundle:Client');
        $applicationRepository = $this->entityManager->getRepository('SparkRepositoryMonitorBundle:Application');

        if (is_null($this->client) === false) {
            $client = $clientRepository->findOneBy(array('code' => $this->client));
            $this->processClientApplications($client);
        } elseif (is_null($this->application) === false) {
            $application = $applicationRepository->findOneBy(
                array('slug' => $this->application)
            );
            $this->processApplication($application);
        } else {
            $clients = $clientRepository->findAll();
            foreach ($clients as $client) {
                $this->processClientApplications($client);
            }
        }

        return 0;
    }

    /**
     * @param Client|null $client
     */
    protected function processClientApplications(Client $client = null)
    {
        if ($client instanceof Client) {
            if ($client->getApplications()->count() > 0) {
                $this->output->writeln(sprintf('Refresh applications for client %s', $client->getName()));
                foreach ($client->getApplications() as $application) {
                    $this->processApplication($application);
                }
            }
        } else {
            $this->output->writeln('Client not found in DB');
        }
    }

    /**
     * @param Application|null $application
     */
    protected function processApplication(Application $application = null)
    {
        if ($application instanceof Application) {
            $this->output->writeln(sprintf('Refresh applications for application %s', $application->getName()));
            $this->producer->process($application);
        } else {
            $this->output->writeln('Application not found in DB');
        }
    }
}
