<?php
/**
 * \file LoginUrlExtension.php
 * \author Pierre TRANCHARD <pierre@tranchard.net>
 * \version 1.0
 * \date 24/11/2015
 * \brief
 * \details
 */

namespace Spark\RepositoryMonitorBundle\Twig\Extension;

use Spark\RepositoryMonitorBundle\DependencyInjection\Configuration;
use Spark\RepositoryMonitorBundle\Services\LoginUrlGenerator;

/**
 * Class LoginUrlExtension
 *
 * @package Spark\RepositoryMonitorBundle\Twig\Extension
 */
class LoginUrlExtension extends \Twig_Extension
{
    
    /**
     * @var LoginUrlGenerator
     */
    protected $generator;
    
    public function __construct(LoginUrlGenerator $generator)
    {
        $this->generator = $generator;
    }
    
    /**
     * {@inheritdoc}
     */
    public function getFunctions()
    {
        return array(
            new \Twig_SimpleFunction('login_url', array($this, 'getLoginUrl')),
            new \Twig_SimpleFunction('login_path', array($this, 'getLoginPath')),
        );
    }
    
    /**
     * Generates the relative login URL.
     *
     * @return string The relative login URL
     */
    public function getLoginPath()
    {
        return $this->generator->getLoginPath();
    }
    
    /**
     * Generates the absolute login URL.
     *
     * @return string The absolute login URL
     */
    public function getLoginUrl()
    {
        return $this->generator->getLoginUrl();
    }
    
    /**
     * Returns the name of the extension.
     *
     * @return string The extension name
     */
    public function getName()
    {
        return sprintf('%s_login_generator_extension', Configuration::getRootNode());
    }
}
