<?php
/**
 * \file PlatformExtension.php
 * \author Pierre TRANCHARD <pierre@tranchard.net>
 * \version 1.0
 * \date 20/07/15
 * \brief
 * \details
 */

namespace Spark\RepositoryMonitorBundle\Twig\Extension;

use Spark\RepositoryMonitorBundle\Manager\ClientManager;

/**
 * Class PlatformExtension
 *
 * @package Spark\RepositoryMonitorBundle\Twig\Extension
 */
class PlatformExtension extends \Twig_Extension
{

    /**
     * @var ClientManager
     */
    protected $clientManager;

    /**
     * @param ClientManager $clientManager
     */
    public function __construct(ClientManager $clientManager)
    {
        $this->clientManager = $clientManager;
    }

    /**
     * @return array
     */
    public function getFunctions()
    {
        return array('getPlatforms' => new \Twig_SimpleFunction('getPlatforms', array($this, 'getPlatforms')));
    }

    /**
     * @return array
     */
    public function getPlatforms()
    {
        return $this->clientManager->getPlatforms();
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'spark_repository_monitor_platform_extension';
    }
}
