<?php
/**
 * \file LinkifyExtension.php
 * \author Pierre TRANCHARD <pierre@tranchard.net>
 * \version 1.0
 * \date 21/06/15
 * \brief
 * \details
 */

namespace Spark\RepositoryMonitorBundle\Twig\Extension;

/**
 * Class LinkifyExtension
 *
 * @package Spark\RepositoryMonitorBundle\Twig\Extension
 */
class LinkifyExtension extends \Twig_Extension
{

    /**
     * @var string
     */
    protected $ticketManagerUri;

    /**
     * @var string
     */
    protected $ticketRegex;

    /**
     * Constructor
     *
     * @param string $ticketManagerUri
     * @param string $ticketRegex
     */
    public function __construct($ticketManagerUri, $ticketRegex)
    {
        $this->ticketManagerUri = $ticketManagerUri;
        $this->ticketRegex      = $ticketRegex;
    }

    /**
     * Get filters
     *
     * @return array
     */
    public function getFilters()
    {
        return array(
            'linkify' => new \Twig_SimpleFilter('linkify', array($this, 'linkifyFilter')),
        );
    }

    /**
     * Linkify
     *
     * @param string $text
     *
     * @return string
     */
    public function linkifyFilter($text)
    {
        $text    = preg_replace('/- \[(.*)/', "[$1", $text);
        $pattern = sprintf('/(%s)/', $this->ticketRegex);

        $replacement = sprintf("<a href=\"%s/browse/$1\" target=\"_blank\">$1</a>", $this->ticketManagerUri);

        return preg_replace($pattern, $replacement, $text);
    }

    /**
     * Extension name
     *
     * @return string
     */
    public function getName()
    {
        return 'spark_repository_monitor_linkify_extension';
    }
}
