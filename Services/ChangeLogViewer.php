<?php
/**
 * \file ChangeLogViewer.php
 * \author Pierre TRANCHARD <pierre@tranchard.net> Benoit Maziere <benoit.maziere@gmail.com>
 * \version 1.0
 * \date 10/07/15
 * \brief
 * \details
 */

namespace Spark\RepositoryMonitorBundle\Services;

use Spark\RepositoryMonitorBundle\Entity\Application;
use Spark\RepositoryMonitorBundle\Process\GitProcess;

/**
 * Class ChangeLogViewer
 *
 * @package Spark\RepositoryMonitorBundle\Services
 */
class ChangeLogViewer
{

    /**
     * @var ProjectDirectoryGuesser
     */
    protected $projectDirectoryGuesser;

    /**
     * @var string
     */
    protected $ticketRegex;

    /**
     * Constructor
     *
     * @param ProjectDirectoryGuesser $projectDirectoryGuesser
     * @param string                  $ticketRegex
     */
    public function __construct(ProjectDirectoryGuesser $projectDirectoryGuesser, $ticketRegex)
    {
        $this->projectDirectoryGuesser = $projectDirectoryGuesser;
        $this->ticketRegex             = $ticketRegex;
    }

    /**
     * @param Application $application
     * @param array       $errors
     *
     * @return array
     */
    public function getTags(Application $application, &$errors)
    {
        try {
            $projectDirectory = $this->projectDirectoryGuesser->guessViaApplication($application);
            $gitProcess       = new GitProcess();
            $tags             = array_merge(
                GitProcess::convertOutputToArray($gitProcess->sha1Process($projectDirectory, $errors)->getOutput()),
                GitProcess::convertOutputToArray(
                    $gitProcess->tagProcess($projectDirectory, $errors)->getOutput()
                )
            );
            if (!empty($tags)) {
                $tags[] = 'HEAD';
            }
        } catch (\Exception $exception) {
            $errors[] = "Git repository not found";
            $tags     = array();
        }

        return $tags;
    }

    /**
     * @param Application $application
     * @param string      $upperTag
     * @param string      $lowerTag
     * @param array       $changeLog
     * @param array       $errors
     *
     * @return string
     */
    public function getChangeLog(Application $application, $upperTag, $lowerTag, &$changeLog, &$errors)
    {
        $gitProcess = new GitProcess();
        try {
            $projectDirectory = $this->projectDirectoryGuesser->guessViaApplication($application);
            $changeLog        = GitProcess::convertOutputToArray(
                $gitProcess->logProcess($projectDirectory, $lowerTag, $upperTag, $errors)->getOutput()
            );
        } catch (\Exception $exception) {
            $errors[] = 'Git repository not found';
        }
        $ticketList = $this->getTicketsList($changeLog, $errors);

        return $ticketList;
    }

    /**
     * Return a list of tickets identifiers from an array of commit descriptions
     *
     * @param array $changeLogs
     * @param array $errors
     *
     * @return string
     */
    protected function getTicketsList($changeLogs, &$errors)
    {
        $ticketsList = array();

        try {
            foreach ($changeLogs as $changeLog) {
                if (preg_match_all(sprintf("/%s/", $this->ticketRegex), $changeLog, $tickets) > 0) {
                    foreach ($tickets as $ticket) {
                        foreach ($ticket as $ticketId) {
                            array_push($ticketsList, $ticketId);
                        }
                    }
                }
            }
        } catch (\Exception $exception) {
            $errors[] = $exception->getMessage();
        }

        return implode(', ', array_unique($ticketsList));
    }
}
