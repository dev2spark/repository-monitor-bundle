<?php
/**
 * \file LoginUrlGenerator.php
 * \author Pierre TRANCHARD <pierre@tranchard.net>
 * \version 1.0
 * \date 24/11/2015
 * \brief
 * \details
 */

namespace Spark\RepositoryMonitorBundle\Services;

use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

/**
 * Class LoginUrlGenerator
 *
 * @package Spark\RepositoryMonitorBundle\Services
 */
class LoginUrlGenerator
{

    /**
     * @var UrlGeneratorInterface
     */
    protected $router;

    /**
     * @var string
     */
    protected $loginPath;

    /**
     * Constructor.
     *
     * @param UrlGeneratorInterface $router
     * @param string|null           $loginPath
     */
    public function __construct(UrlGeneratorInterface $router, $loginPath = null)
    {
        $this->router    = $router;
        $this->loginPath = is_null($loginPath) ? '' : $loginPath;
    }

    /**
     * @return string
     */
    public function getLoginPath()
    {
        return $this->generateLoginUrl(UrlGeneratorInterface::ABSOLUTE_PATH);
    }

    /**
     * @return string
     */
    public function getLoginUrl()
    {
        return $this->generateLoginUrl(UrlGeneratorInterface::ABSOLUTE_URL);
    }

    /**
     * @param bool $referenceType
     *
     * @return null|string
     */
    protected function generateLoginUrl($referenceType = UrlGeneratorInterface::ABSOLUTE_URL)
    {
        if ($this->loginPath[0] === '/') {
            return $this->loginPath;
        }

        return $this->router->generate($this->loginPath, array(), $referenceType);
    }
}
