<?php
/**
 * \file ProjectDirectoryGuesser.php
 * \author Pierre TRANCHARD <pierre@tranchard.net>
 * \version 1.0
 * \date 09/07/15
 * \brief
 * \details
 */

namespace Spark\RepositoryMonitorBundle\Services;

use Spark\RepositoryMonitorBundle\Entity\Application;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class ProjectDirectoryGuesser
 *
 * @package Spark\RepositoryMonitorBundle\Services
 */
class ProjectDirectoryGuesser
{

    /**
     * @var array
     */
    protected $baseFolders;

    /**
     * Constructor
     *
     * @param array $baseFolders
     */
    public function __construct(array $baseFolders = array())
    {
        $this->baseFolders = $baseFolders;
    }

    /**
     * @param Application $application
     *
     * @return string
     * @throws NotFoundHttpException
     */
    public function guessViaApplication(Application $application)
    {
        $directory    = $application->getDirectory();
        $appDirectory = sprintf("~%s%s", DIRECTORY_SEPARATOR, $directory);
        if (realpath($appDirectory) !== false) {
            return realpath($appDirectory);
        }
        $separator = array_intersect($this->baseFolders, explode(DIRECTORY_SEPARATOR, __DIR__));
        if (!empty($separator)) {
            $separator = end($separator);
        } else {
            throw new NotFoundHttpException();
        }
        $baseProjectPath = explode($separator, __DIR__, 2);
        $appDirectory    = sprintf("%s%s", reset($baseProjectPath), $separator);

        return realpath(sprintf("%s%s%s", $appDirectory, DIRECTORY_SEPARATOR, $directory));
    }
}
