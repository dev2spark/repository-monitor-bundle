<?php
/**
 * \file RefreshDeploymentProducer.php
 * \author Pierre TRANCHARD <pierre@tranchard.net>
 * \version 1.0
 * \date 30/06/15
 * \brief
 * \details
 */

namespace Spark\RepositoryMonitorBundle\Services;

use OldSound\RabbitMqBundle\RabbitMq\Producer;
use Psr\Log\LoggerInterface;
use Spark\FrameworkBundle\Traits\Logger;
use Spark\RepositoryMonitorBundle\Entity\Application;

/**
 * Class RefreshDeploymentProducer
 *
 * @package Spark\RepositoryMonitorBundle\Services
 */
class RefreshDeploymentProducer
{

    /**
     * @var Producer
     */
    protected $producer;

    /**
     * @var ProjectDirectoryGuesser
     */
    protected $projectDirectoryGuesser;

    use Logger;

    /**
     * Constructor
     *
     * @param Producer                $producer
     * @param ProjectDirectoryGuesser $projectDirectoryGuesser
     * @param LoggerInterface         $logger
     */
    public function __construct(
        Producer $producer,
        ProjectDirectoryGuesser $projectDirectoryGuesser,
        LoggerInterface $logger = null
    ) {
        $this->producer                = $producer;
        $this->projectDirectoryGuesser = $projectDirectoryGuesser;
        $this->setLogger("deployment_producer", $logger);
    }

    /**
     * @param Application $application
     */
    public function process(Application $application)
    {
        try {
            $projectDirectory = $this->projectDirectoryGuesser->guessViaApplication($application);
        } catch (\Exception $exception) {
            $projectDirectory = null;
        }

        $message = array(
            'id'                => $application->getId(),
            'slug'              => $application->getSlug(),
            'uri'               => $application->getUri(),
            'client'            => $application->getClient()->getName(),
            'project_directory' => $projectDirectory,
        );

        $this->producer->publish(json_encode($message));
    }
}