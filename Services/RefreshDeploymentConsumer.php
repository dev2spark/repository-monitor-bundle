<?php
/**
 * \file RefreshDeploymentConsumer.php
 * \author Pierre TRANCHARD <pierre@tranchard.net>
 * \version 1.0
 * \date 30/06/15
 * \brief
 * \details
 */

namespace Spark\RepositoryMonitorBundle\Services;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\DBALException;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Response;
use OldSound\RabbitMqBundle\RabbitMq\ConsumerInterface;
use PhpAmqpLib\Message\AMQPMessage;
use Psr\Log\LoggerInterface;
use Spark\FrameworkBundle\Component\Memory;
use Spark\FrameworkBundle\Traits\ConnectionLogger;
use Spark\FrameworkBundle\Traits\Logger;
use Spark\FrameworkBundle\Traits\PDOTablePrefix;
use Spark\RepositoryMonitorBundle\Process\GitProcess;

/**
 * Class RefreshDeploymentConsumer
 *
 * @package Spark\RepositoryMonitorBundle\Services
 */
class RefreshDeploymentConsumer implements ConsumerInterface
{

    use ConnectionLogger;

    use PDOTablePrefix;

    use Logger;

    /**
     * @var Connection
     */
    protected $connection;

    /**
     * Constructor
     *
     * @param Connection      $connection
     * @param string|null     $tablePrefix
     * @param LoggerInterface $logger
     */
    public function __construct(Connection $connection, $tablePrefix = null, LoggerInterface $logger = null)
    {
        $this->connection  = $this->disableConnectionLogger($connection);
        $this->tablePrefix = $tablePrefix;
        $this->setLogger("refresh_deployment_consumer", $logger);
    }

    /**
     * @param AMQPMessage $msg The message
     *
     * @return mixed false to reject and requeue, any other value to acknowledge
     */
    public function execute(AMQPMessage $msg)
    {
        $this->info("Receive a message");
        $this->warning(sprintf("Memory usage : %s", Memory::memoryUsage()));

        $messageBody = json_decode($msg->body, true);
        unset($msg);
        if (is_array($messageBody) && empty($messageBody) === false) {
            $applicationId  = $messageBody['id'];
            $currentVersion = $this->findLastDeploymentVersion($applicationId);
            if (is_null($messageBody['uri']) === false) {
                $config = array('base_uri' => $messageBody['uri'], 'timeout' => 5, 'verify' => false);
                $client = new Client($config);
                try {
                    $versionPath = '/version';
                    $basePath    = parse_url($config['base_uri'], PHP_URL_PATH);
                    if (is_null($basePath) === false) {
                        $versionPath = sprintf("%s%s", $basePath, $versionPath);
                    }
                    /** @var Response $response */
                    $response      = $client->request('GET', $versionPath);
                    $onlineVersion = json_decode($response->getBody()->getContents(), true);
                    $isOutDated    = true;
                    if (isset($onlineVersion['date']) && isset($currentVersion['date'])) {
                        $online  = \DateTime::createFromFormat("Y-m-d H:i:s", $onlineVersion['date']);
                        $current = \DateTime::createFromFormat("Y-m-d H:i:s", $currentVersion['date']);

                        if ($online instanceof \DateTime && $current instanceof \DateTime) {
                            $difference = $online->diff($current);
                            $isOutDated = ($difference->y > 0 || $difference->m > 0 || $difference->d > 0 || $difference->h > 0 || $difference->i > 0 || $difference->s > 0);
                        }
                    }
                    if ($isOutDated) {
                        $this->addDeploymentVersion($onlineVersion, $applicationId);
                    }
                } catch (\Exception $exception) {
                    $this->error($exception->getMessage());
                    $this->warning(sprintf("Memory usage : %s", Memory::memoryUsage()));
                }
            } else {
                $projectDirectory = $messageBody['project_directory'];
                if (is_null($projectDirectory) === false && $projectDirectory !== false) {
                    try {
                        $errors          = array();
                        $gitProcess      = new GitProcess();
                        $tags            = GitProcess::convertOutputToArray(
                            $gitProcess->tagProcess($projectDirectory, $errors, false)->getOutput()
                        );
                        $mostRecentTag   = end($tags);
                        $tagData         = explode(" ", $mostRecentTag);
                        $tag             = $tagData[3];
                        $tagCreationDate = \DateTime::createFromFormat(
                            "Y-m-d H:i:s",
                            sprintf("%s %s", $tagData[0], $tagData[1])
                        );
                        $timeZone        = new \DateTimeZone($tagData[2]);
                        $tagCreationDate->setTimezone($timeZone);

                        $tagDetails = $gitProcess->convertOutputToArray(
                            $gitProcess->showRefsTags($projectDirectory, $errors, $tag)->getOutput()
                        );

                        $gitProcess        = null;
                        $repositoryVersion = array(
                            'build'       => $tag,
                            'date'        => $tagCreationDate->format('Y-m-d H:i:s'),
                            'deployment'  => "standalone",
                            'environment' => 'development',
                            'revision'    => substr(explode(" ", end($tagDetails))[0], 0, 7),
                        );
                        if (empty($currentVersion) || $currentVersion['build'] < $repositoryVersion['build']) {
                            $this->addDeploymentVersion($repositoryVersion, $applicationId);
                        }
                    } catch (\Exception $exception) {
                        $this->error($exception->getMessage());
                        $this->warning(sprintf("Memory usage : %s", Memory::memoryUsage()));
                    }
                }
            }
        }

        return ConsumerInterface::MSG_ACK;
    }

    /**
     * @param $applicationId
     *
     * @return array
     */
    protected function findLastDeploymentVersion($applicationId)
    {
        $this->connection->connect();
        $version = array();
        try {
            $query     = sprintf(
                "SELECT `id` `application_id`, `build`, `date`, `revision` FROM %s WHERE `application_id` = :application_id ORDER BY `id` DESC LIMIT 1",
                $this->getTableName('version')
            );
            $statement = $this->connection->prepare($query);
            $statement->bindValue('application_id', $applicationId);
        } catch (DBALException $exception) {
            $this->error($exception->getMessage());
            $this->warning(sprintf("Memory usage : %s", Memory::memoryUsage()));
            $statement = null;
        }
        if (is_null($statement) === false) {
            try {
                $statement->execute();
                $version = $statement->fetch(\PDO::FETCH_ASSOC);
                if ($version === false) {
                    $version = array();
                }
            } catch (\Exception $exception) {
                $this->error($exception->getMessage());
                $this->warning(sprintf("Memory usage : %s", Memory::memoryUsage()));
            }
        }
        unset($statement);
        $this->connection->close();

        return $version;
    }

    /**
     * @param array $onlineVersion
     * @param int   $applicationId
     *
     * @return bool
     */
    protected function addDeploymentVersion(array $onlineVersion, $applicationId)
    {
        $this->connection->connect();
        $hasSucceed = false;
        try {
            $query     = sprintf(
                "INSERT INTO %s (application_id, build, date, revision) VALUES (:application_id, :build, :date, :revision)",
                $this->getTableName('version')
            );
            $statement = $this->connection->prepare($query);
            $statement->bindValue('application_id', $applicationId);
            $statement->bindValue('build', $onlineVersion['build']);
            $statement->bindValue('date', $onlineVersion['date']);
            $statement->bindValue('revision', $onlineVersion['revision']);
        } catch (DBALException $exception) {
            $this->error($exception->getMessage());
            $this->warning(sprintf("Memory usage : %s", Memory::memoryUsage()));
            $statement = null;
        }
        if (is_null($statement) === false) {
            try {
                $hasSucceed = $statement->execute();
            } catch (\Exception $exception) {
                $this->error($exception->getMessage());
                $this->warning(sprintf("Memory usage : %s", Memory::memoryUsage()));
            }
        }
        unset($statement);
        $this->connection->close();

        return $hasSucceed;
    }
}
