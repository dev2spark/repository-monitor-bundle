if (jQuery) {
    jQuery(document).ready(function ($) {
        var projectView = new ProjectView($("#results"));
    });

}
/**
 *
 * @param dom
 * @constructor
 */
function ProjectView(dom) {
    this.table = dom;
    this.ajaxUrl = this.table.attr('data-url');
    this.remainingTags = JSON.parse(this.table.attr('data-tags'));
    this.showMoreButton = jQuery('#showMore');
    this.displayShowMoreButton();
    this.showMoreButtonEvents();
}

/**
 * Handle show more button events
 */
ProjectView.prototype.showMoreButtonEvents = function () {
    var context = this;
    this.showMoreButton.click(context, function (e) {
        var tagCouple = context.getNextTags();
        context.displayShowMoreButton();
        var tags = tagCouple.split(" - ");
        var lowerTag = tags[0];
        var upperTag = tags[1];

        context.renderView(upperTag, lowerTag);
    });
};

/**
 *
 * @returns {*}
 */
ProjectView.prototype.getNextTags = function () {
    var currentCouple = this.remainingTags[0];
    this.remainingTags.shift();
    this.table.attr('data-tags', this.remainingTags);

    return currentCouple;
};

/**
 *
 * @param upperTag
 * @param lowerTag
 */
ProjectView.prototype.renderView = function (upperTag, lowerTag) {
    var context = this;
    var url = context.ajaxUrl.replace('upper', upperTag);
    url = url.replace('lower', lowerTag);

    jQuery.ajax({
        url: url,
        context: context,
        success: function (data) {
            context.table.find(".table-row").last().after(data);
        },
        error: function (err) {
            console.log(err);
        }
    })
};

/**
 *
 * @returns {boolean}
 */
ProjectView.prototype.displayShowMoreButton = function () {

    if (this.remainingTags.length > 0) {
        this.showMoreButton.show();

        return true;
    }

    this.showMoreButton.hide();

    return false;
};