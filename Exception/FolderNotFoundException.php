<?php
/**
 * \file FolderNotFoundException.php
 * \author Pierre TRANCHARD <pierre@tranchard.net>
 * \version 1.0
 * \date 09/07/15
 * \brief
 * \details
 */

namespace Spark\RepositoryMonitorBundle\Exception;

/**
 * Class FolderNotFoundException
 *
 * @package Spark\RepositoryMonitorBundle\Exception
 */
class FolderNotFoundException extends \Exception
{

}