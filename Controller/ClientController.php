<?php
/**
 * \file ClientController.php
 * \author Pierre TRANCHARD <pierre@tranchard.net>
 * \version 1.0
 * \date 26/06/15
 * \brief
 * \details
 */

namespace Spark\RepositoryMonitorBundle\Controller;

use Spark\RepositoryMonitorBundle\Entity\Application;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

/**
 * Class ClientController
 *
 * @package Spark\RepositoryMonitorBundle\Controller
 *
 * @Route("/client")
 */
class ClientController extends Controller
{

    /**
     * @Route("/dashboard", name="spark_repository_monitor_client_list")
     * @Method({"GET"})
     *
     * @param Request $request
     *
     * @return Response
     */
    public function dashboardAction(Request $request)
    {
        return $this->render(
            '@SparkRepositoryMonitor/layout.html.twig'
        );
    }

    /**
     * @Route("/refresh-deployment-status", name="spark_repository_monitor_client_refresh_deployment_status")
     * @Route("/refresh-deployment-status/{slug}", name="spark_repository_monitor_client_refresh_deployment_status_application")
     * @ParamConverter("application", class="SparkRepositoryMonitorBundle:Application", options={"slug"="slug"})
     * @Method({"GET"})
     *
     * @param Request     $request
     * @param Application $application
     *
     * @return RedirectResponse
     */
    public function refreshAction(Request $request, Application $application = null)
    {
        $deploymentProducer = $this->get('spark_repository_monitor.services.refresh_deployment_producer');
        if (is_null($application)) {
            $applications = $this->get('doctrine.orm.entity_manager')->getRepository(
                'SparkRepositoryMonitorBundle:Application'
            )->findAll();
            $url          = $this->generateUrl('spark_repository_monitor_client_list');
        } else {
            $applications = array($application);
            $url          = $this->generateUrl(
                'spark_repository_monitor_application_display',
                array('slug' => $application->getSlug())
            );
        }
        foreach ($applications as $application) {
            $deploymentProducer->process($application);
        }

        return $this->redirect($url);
    }
}
