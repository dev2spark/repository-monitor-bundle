<?php
/**
 * \file ApplicationController.php
 * \author Pierre TRANCHARD <pierre@tranchard.net>
 * \version 1.0
 * \date 09/07/15
 * \brief
 * \details
 */

namespace Spark\RepositoryMonitorBundle\Controller;

use Spark\RepositoryMonitorBundle\Entity\Application;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class ApplicationController
 *
 * @package Spark\RepositoryMonitorBundle\Controller
 *
 * @Route("/application")
 */
class ApplicationController extends Controller
{

    /**
     * @Route("/{slug}", name="spark_repository_monitor_application_display")
     * @ParamConverter("application", class="SparkRepositoryMonitorBundle:Application", options={"slug"="slug"})
     * @Method({"GET"})
     *
     * @param Request     $request
     * @param Application $application
     *
     * @return Response
     */
    public function displayAction(Request $request, Application $application)
    {
        $defaultShownTag = $this->getParameter('spark_repository_monitor.default_shown_tags');
        $changeLogViewer = $this->get('spark_repository_monitor.services.change_log_viewer');
        $errors          = array();
        $tags            = array_reverse($changeLogViewer->getTags($application, $errors));
        $tagsCoupled     = array();

        for ($i = 0; $i < min(count($tags) - 1, $defaultShownTag); $i++) {
            $changeLog     = array();
            $lowerTag      = $tags[$i + 1];
            $upperTag      = $tags[$i];
            $couple        = sprintf("%s - %s", $lowerTag, $upperTag);
            $ticketList    = $changeLogViewer->getChangeLog(
                $application,
                $upperTag,
                $lowerTag,
                $changeLog,
                $errors
            );
            $tagsCoupled[] = array($couple => array('tickets' => $ticketList, 'changeLog' => $changeLog));
        }

        $remainingTags = array();
        for ($i = $defaultShownTag; $i < count($tags) - 1; $i++) {
            $lowerTag        = $tags[$i + 1];
            $upperTag        = $tags[$i];
            $couple          = sprintf("%s - %s", $lowerTag, $upperTag);
            $remainingTags[] = $couple;
        }

        return $this->render(
            '@SparkRepositoryMonitor/Application/display.html.twig',
            array(
                'application'      => $application,
                'errors'           => $errors,
                'tags'             => $tagsCoupled,
                'defaultShownTags' => $defaultShownTag,
                'remainingTags'    => $remainingTags,
            )
        );
    }

    /**
     * @Route("/changeLog/{slug}/{upperTag}/{lowerTag}", name="spark_repository_monitor_application_ajax_change_log")
     * @ParamConverter("application", class="SparkRepositoryMonitorBundle:Application", options={"slug"="slug"})
     * @Method({"GET"})
     *
     * @param Application $application
     * @param string      $upperTag
     * @param string      $lowerTag
     *
     * @return Response
     */
    public function ajaxChangeLogAction(Application $application, $upperTag, $lowerTag)
    {
        $coupleTag       = sprintf("%s - %s", $lowerTag, $upperTag);
        $changeLogViewer = $this->get('spark_repository_monitor.services.change_log_viewer');
        $errors          = array();
        $ticketsList     = $changeLogViewer->getChangeLog($application, $upperTag, $lowerTag, $changeLog, $errors);

        return $this->render(
            '@SparkRepositoryMonitor/Application/_change_log_row.html.twig',
            array(
                'upperTag' => $upperTag,
                'lowerTag' => $lowerTag,
                'tag'      => $coupleTag,
                'data'     => array('tickets' => $ticketsList, 'changeLog' => $changeLog),
                'errors'   => $errors,
            )
        );
    }
}
