<?php
/**
 * \file ApplicationController.php
 * \author Pierre TRANCHARD <pierre@tranchard.net>
 * \version 1.0
 * \date 20/06/15
 * \brief
 * \details
 */

namespace Spark\RepositoryMonitorBundle\Controller\Admin;

use Knp\Component\Pager\PaginatorInterface;
use Spark\RepositoryMonitorBundle\Entity\Application;
use Spark\RepositoryMonitorBundle\Form\Handler\ApplicationHandler;
use Spark\RepositoryMonitorBundle\Manager\ApplicationManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

/**
 * Class ApplicationController
 *
 * @package Spark\RepositoryMonitorBundle\Controller\Admin
 *
 * @Route("/application", service="spark_repository_monitor.controller.admin.application")
 */
class ApplicationController extends Controller
{

    /**
     * @var ApplicationManager
     */
    protected $manager;

    /**
     * @var ApplicationHandler
     */
    protected $formHandler;

    /**
     * @var PaginatorInterface
     */
    protected $paginator;

    /**
     * @var Session
     */
    protected $session;

    /**
     * Constructor
     *
     * @param ApplicationManager $manager
     * @param ApplicationHandler $formHandler
     * @param PaginatorInterface $paginator
     * @param Session            $session
     */
    public function __construct(
        ApplicationManager $manager,
        ApplicationHandler $formHandler,
        PaginatorInterface $paginator,
        Session $session
    ) {
        $this->manager     = $manager;
        $this->formHandler = $formHandler;
        $this->paginator   = $paginator;
        $this->session     = $session;
    }

    /**
     * @Route("/create", name = "spark_repository_monitor_admin_application_create")
     * @Method({"GET", "POST"})
     *
     * @param Request $request
     *
     * @return Response
     */
    public function createAction(Request $request)
    {
        $application = $this->manager->createInstance();

        if ($this->formHandler->handleRequest($application)) {
            $this->session->getFlashBag()->add(
                'success',
                sprintf("%s was successfully created", $application->getName())
            );

            return $this->redirectToRoute('spark_repository_monitor_admin_application_list');
        }

        return $this->render(
            '@SparkRepositoryMonitor/Admin/Application/create.html.twig',
            array('form' => $this->formHandler->getForm()->createView())
        );
    }

    /**
     * @Route("/edit/{slug}", name = "spark_repository_monitor_admin_application_edit")
     * @Method({"GET", "POST"})
     * @ParamConverter("application", class="SparkRepositoryMonitorBundle:Application", options={"slug"="slug"})
     *
     * @param Request     $request
     * @param Application $application
     *
     * @return Response
     */
    public function editAction(Request $request, Application $application)
    {
        if ($this->formHandler->handleRequest($application)) {
            $this->session->getFlashBag()->add(
                'info',
                sprintf("%s was successfully updated", $application->getName())
            );

            return $this->redirectToRoute('spark_repository_monitor_admin_application_list');
        }

        return $this->render(
            '@SparkRepositoryMonitor/Admin/Application/edit.html.twig',
            array('form' => $this->formHandler->getForm()->createView(), 'application' => $application)
        );
    }

    /**
     * @Route("/list/{page}", name = "spark_repository_monitor_admin_application_list", requirements={"page"="\d+"},
     *                        defaults={"page" = 1}))
     * @Method({"GET"})
     *
     * @param Request $request
     * @param int     $page
     *
     * @return Response
     */
    public function listAction(Request $request, $page = 1)
    {
        $pagination = $this->paginator->paginate($this->manager->getRepository()->findAllQueryBuilder(), $page, 10);

        return $this->render(
            '@SparkRepositoryMonitor/Admin/Application/list.html.twig',
            array('applications' => $pagination)
        );
    }

    /**
     * @Route("/delete/{slug}", name = "spark_repository_monitor_admin_application_delete")
     * @Method({"GET"})
     * @ParamConverter("application", class="SparkRepositoryMonitorBundle:Application", options={"slug"="slug"})
     *
     * @param Request     $request
     * @param Application $application
     *
     * @return RedirectResponse
     * @throws \Exception
     */
    public function deleteAction(Request $request, Application $application)
    {
        $name = $application->getName();
        if ($this->manager->deleteInstance($application)) {
            $this->session->getFlashBag()->add('warning', sprintf("%s was successfully deleted", $name));
        } else {
            $this->session->getFlashBag()->add(
                'danger',
                sprintf("An error occurred when you tried to delete %s", $name)
            );
        }

        return $this->redirectToRoute('spark_repository_monitor_admin_application_list');
    }
}
