<?php
/**
 * \file RepositoryController.php
 * \author Pierre TRANCHARD <pierre@tranchard.net>
 * \version 1.0
 * \date 13/07/15
 * \brief
 * \details
 */

namespace Spark\RepositoryMonitorBundle\Controller\Admin;

use Knp\Component\Pager\PaginatorInterface;
use Spark\RepositoryMonitorBundle\Entity\Repository;
use Spark\RepositoryMonitorBundle\Form\Handler\RepositoryHandler;
use Spark\RepositoryMonitorBundle\Manager\RepositoryManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

/**
 * Class RepositoryController
 *
 * @package Spark\RepositoryMonitorBundle\Controller\Admin
 *
 * @Route("/repository", service="spark_repository_monitor.controller.admin.repository")
 */
class RepositoryController extends Controller
{

    /**
     * @var RepositoryManager
     */
    protected $manager;

    /**
     * @var RepositoryHandler
     */
    protected $formHandler;

    /**
     * @var PaginatorInterface
     */
    protected $paginator;

    /**
     * @var Session
     */
    protected $session;

    /**
     * Constructor
     *
     * @param RepositoryManager  $manager
     * @param RepositoryHandler  $formHandler
     * @param PaginatorInterface $paginator
     * @param Session            $session
     */
    public function __construct(
        RepositoryManager $manager,
        RepositoryHandler $formHandler,
        PaginatorInterface $paginator,
        Session $session
    ) {
        $this->manager     = $manager;
        $this->formHandler = $formHandler;
        $this->paginator   = $paginator;
        $this->session     = $session;
    }

    /**
     * @Route("/create", name = "spark_repository_monitor_admin_repository_create")
     * @Method({"GET", "POST"})
     *
     * @param Request $request
     *
     * @return Response
     */
    public function createAction(Request $request)
    {
        $repository = $this->manager->createInstance();

        if ($this->formHandler->handleRequest($repository)) {
            $this->session->getFlashBag()->add(
                'success',
                sprintf("%s was successfully created", $repository->getUri())
            );

            return $this->redirectToRoute('spark_repository_monitor_admin_repository_list');
        }

        return $this->render(
            '@SparkRepositoryMonitor/Admin/Repository/create.html.twig',
            array('form' => $this->formHandler->getForm()->createView())
        );
    }

    /**
     * @Route("/edit/{id}", name = "spark_repository_monitor_admin_repository_edit")
     * @Method({"GET", "POST"})
     * @ParamConverter("repository", class="SparkRepositoryMonitorBundle:Repository", options={"id"="id"})
     *
     * @param Request    $request
     * @param Repository $repository
     *
     * @return Response
     */
    public function editAction(Request $request, Repository $repository)
    {
        if ($this->formHandler->handleRequest($repository)) {
            $this->session->getFlashBag()->add(
                'info',
                sprintf("%s was successfully updated", $repository->getUri())
            );

            return $this->redirectToRoute('spark_repository_monitor_admin_application_list');
        }

        return $this->render(
            '@SparkRepositoryMonitor/Admin/Repository/edit.html.twig',
            array('form' => $this->formHandler->getForm()->createView(), 'repository' => $repository)
        );
    }

    /**
     * @Route("/list/{page}", name = "spark_repository_monitor_admin_repository_list", requirements={"page"="\d+"},
     *                        defaults={"page" = 1}))
     * @Method({"GET"})
     *
     * @param Request $request
     * @param int     $page
     *
     * @return Response
     */
    public function listAction(Request $request, $page = 1)
    {
        $pagination = $this->paginator->paginate($this->manager->getRepository()->findAll(), $page);

        return $this->render(
            '@SparkRepositoryMonitor/Admin/Repository/list.html.twig',
            array('repositories' => $pagination)
        );
    }

    /**
     * @Route("/delete/{id}", name = "spark_repository_monitor_admin_repository_delete")
     * @Method({"GET"})
     * @ParamConverter("repository", class="SparkRepositoryMonitorBundle:Repository", options={"id"="id"})
     *
     * @param Request    $request
     * @param Repository $repository
     *
     * @return RedirectResponse
     * @throws \Exception
     */
    public function deleteAction(Request $request, Repository $repository)
    {
        $uri = $repository->getUri();
        if ($this->manager->deleteInstance($repository)) {
            $this->session->getFlashBag()->add('warning', sprintf("%s was successfully deleted", $uri));
        } else {
            $this->session->getFlashBag()->add(
                'danger',
                sprintf("An error occurred when you tried to delete %s", $uri)
            );
        }

        return $this->redirectToRoute('spark_repository_monitor_admin_repository_list');
    }
}
