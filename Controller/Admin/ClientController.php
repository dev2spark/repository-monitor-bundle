<?php
/**
 * \file ClientController.php
 * \author Pierre TRANCHARD <pierre@tranchard.net>
 * \version 1.0
 * \date 26/06/15
 * \brief
 * \details
 */

namespace Spark\RepositoryMonitorBundle\Controller\Admin;

use Knp\Component\Pager\PaginatorInterface;
use Spark\RepositoryMonitorBundle\Entity\Client;
use Spark\RepositoryMonitorBundle\Form\Handler\ClientHandler;
use Spark\RepositoryMonitorBundle\Manager\ClientManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;

/**
 * Class ClientController
 *
 * @package Spark\RepositoryMonitorBundle\Controller\Admin
 *
 * @Route("/client", service="spark_repository_monitor.controller.admin.client")
 */
class ClientController extends Controller
{

    /**
     * @var ClientManager
     */
    protected $manager;

    /**
     * @var ClientHandler
     */
    protected $formHandler;

    /**
     * @var PaginatorInterface
     */
    protected $paginator;

    /**
     * @var Session
     */
    protected $session;

    /**
     * Constructor
     *
     * @param ClientManager      $manager
     * @param ClientHandler      $formHandler
     * @param PaginatorInterface $paginator
     * @param Session            $session
     */
    public function __construct(
        ClientManager $manager,
        ClientHandler $formHandler,
        PaginatorInterface $paginator,
        Session $session
    ) {
        $this->manager = $manager;
        $this->formHandler = $formHandler;
        $this->paginator = $paginator;
        $this->session = $session;
    }

    /**
     * @Route("/create", name = "spark_repository_monitor_admin_client_create")
     * @Method({"GET", "POST"})
     *
     * @param Request $request
     *
     * @return Response
     */
    public function createAction(Request $request)
    {
        $client = $this->manager->createInstance();

        if ($this->formHandler->handleRequest($client)) {
            $this->session->getFlashBag()->add(
                'success',
                sprintf("%s was successfully created", $client->getName())
            );

            return $this->redirectToRoute('spark_repository_monitor_admin_client_list');
        }

        return $this->render(
            '@SparkRepositoryMonitor/Admin/Client/create.html.twig',
            array('form' => $this->formHandler->getForm()->createView())
        );
    }

    /**
     * @Route("/edit/{code}", name = "spark_repository_monitor_admin_client_edit")
     * @Method({"GET", "POST"})
     * @ParamConverter("client", class="SparkRepositoryMonitorBundle:Client", options={"code"="code"})
     *
     * @param Request $request
     * @param Client  $client
     *
     * @return Response
     */
    public function editAction(Request $request, Client $client)
    {
        if ($this->formHandler->handleRequest($client)) {
            $this->session->getFlashBag()->add(
                'info',
                sprintf("%s was successfully updated", $client->getName())
            );

            return $this->redirectToRoute('spark_repository_monitor_admin_client_list');
        }

        return $this->render(
            '@SparkRepositoryMonitor/Admin/Client/edit.html.twig',
            array('form' => $this->formHandler->getForm()->createView(), 'client' => $client)
        );
    }

    /**
     * @Route("/list/{page}", name = "spark_repository_monitor_admin_client_list", requirements={"page"="\d+"},
     *                        defaults={"page" = 1})
     * @Method({"GET"})
     *
     * @param Request $request
     * @param int     $page
     *
     * @return Response
     */
    public function listAction(Request $request, $page = 1)
    {
        $pagination = $this->paginator->paginate($this->manager->getRepository()->findAll(), $page);

        return $this->render(
            '@SparkRepositoryMonitor/Admin/Client/list.html.twig',
            array('clients' => $pagination)
        );
    }

    /**
     * @Route("/delete/{code}", name = "spark_repository_monitor_admin_client_delete")
     * @Method({"GET"})
     * @ParamConverter("client", class="SparkRepositoryMonitorBundle:Client", options={"code"="code"})
     *
     * @param Request $request
     * @param Client  $client
     *
     * @return RedirectResponse
     * @throws \Exception
     */
    public function deleteAction(Request $request, Client $client)
    {
        $name = $client->getName();
        if ($this->manager->deleteInstance($client)) {
            $this->session->getFlashBag()->add('warning', sprintf("%s was successfully deleted", $name));
        } else {
            $this->session->getFlashBag()->add(
                'danger',
                sprintf("An error occurred when you tried to delete %s", $name)
            );
        }

        return $this->redirectToRoute('spark_repository_monitor_admin_client_list');
    }
}
