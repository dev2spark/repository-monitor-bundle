<?php
/**
 * \file VcsProviderController.php
 * \author Pierre TRANCHARD <pierre@tranchard.net>
 * \version 1.0
 * \date 13/07/15
 * \brief
 * \details
 */

namespace Spark\RepositoryMonitorBundle\Controller\Admin;

use Knp\Component\Pager\PaginatorInterface;
use Spark\RepositoryMonitorBundle\Entity\VcsProvider;
use Spark\RepositoryMonitorBundle\Form\Handler\VcsProviderHandler;
use Spark\RepositoryMonitorBundle\Manager\VcsProviderManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;

/**
 * Class VcsProviderController
 *
 * @package Spark\RepositoryMonitorBundle\Controller\Admin
 *
 * @Route("/vcsProvider", service="spark_repository_monitor.controller.admin.vcs_provider")
 */
class VcsProviderController extends Controller
{

    /**
     * @var VcsProviderManager
     */
    protected $manager;

    /**
     * @var VcsProviderHandler
     */
    protected $formHandler;

    /**
     * @var PaginatorInterface
     */
    protected $paginator;

    /**
     * @var Session
     */
    protected $session;

    /**
     * Constructor
     *
     * @param VcsProviderManager $manager
     * @param VcsProviderHandler $formHandler
     * @param PaginatorInterface $paginator
     * @param Session            $session
     */
    public function __construct(
        VcsProviderManager $manager,
        VcsProviderHandler $formHandler,
        PaginatorInterface $paginator,
        Session $session
    ) {
        $this->manager     = $manager;
        $this->formHandler = $formHandler;
        $this->paginator   = $paginator;
        $this->session     = $session;
    }

    /**
     * @Route("/create", name = "spark_repository_monitor_admin_vcs_provider_create")
     * @Method({"GET", "POST"})
     *
     * @param Request $request
     *
     * @return Response
     */
    public function createAction(Request $request)
    {
        $vcsProvider = $this->manager->createInstance();

        if ($this->formHandler->handleRequest($vcsProvider)) {
            $this->session->getFlashBag()->add(
                'success',
                sprintf("%s was successfully created", $vcsProvider->getName())
            );

            return $this->redirectToRoute('spark_repository_monitor_admin_vcs_provider_list');
        }

        return $this->render(
            '@SparkRepositoryMonitor/Admin/VcsProvider/create.html.twig',
            array('form' => $this->formHandler->getForm()->createView())
        );
    }

    /**
     * @Route("/edit/{id}", name = "spark_repository_monitor_admin_vcs_provider_edit")
     * @Method({"GET", "POST"})
     * @ParamConverter("repository", class="SparkRepositoryMonitorBundle:VcsProvider", options={"id"="id"})
     *
     * @param Request     $request
     * @param VcsProvider $vcsProvider
     *
     * @return Response
     */
    public function editAction(Request $request, VcsProvider $vcsProvider)
    {
        if ($this->formHandler->handleRequest($vcsProvider)) {
            $this->session->getFlashBag()->add(
                'info',
                sprintf("%s was successfully updated", $vcsProvider->getUri())
            );

            return $this->redirectToRoute('spark_repository_monitor_admin_vcs_provider_list');
        }

        return $this->render(
            '@SparkRepositoryMonitor/Admin/VcsProvider/edit.html.twig',
            array('form' => $this->formHandler->getForm()->createView(), 'vcsProvider' => $vcsProvider)
        );
    }

    /**
     * @Route("/list/{page}", name = "spark_repository_monitor_admin_vcs_provider_list", requirements={"page"="\d+"},
     *                        defaults={"page" = 1}))
     * @Method({"GET"})
     *
     * @param Request $request
     * @param int     $page
     *
     * @return Response
     */
    public function listAction(Request $request, $page = 1)
    {
        $pagination = $this->paginator->paginate($this->manager->getRepository()->findAll(), $page);

        return $this->render(
            '@SparkRepositoryMonitor/Admin/VcsProvider/list.html.twig',
            array('vcsProviders' => $pagination)
        );
    }

    /**
     * @Route("/delete/{id}", name = "spark_repository_monitor_admin_vcs_provider_delete")
     * @Method({"GET"})
     * @ParamConverter("repository", class="SparkRepositoryMonitorBundle:VcsProvider", options={"id"="id"})
     *
     * @param Request     $request
     * @param VcsProvider $vcsProvider
     *
     * @return RedirectResponse
     * @throws \Exception
     */
    public function deleteAction(Request $request, VcsProvider $vcsProvider)
    {
        $name = $vcsProvider->getName();
        if ($this->manager->deleteInstance($vcsProvider)) {
            $this->session->getFlashBag()->add('warning', sprintf("%s was successfully deleted", $name));
        } else {
            $this->session->getFlashBag()->add(
                'danger',
                sprintf("An error occurred when you tried to delete %s", $name)
            );
        }

        return $this->redirectToRoute('spark_repository_monitor_admin_vcs_provider_list');
    }
}
