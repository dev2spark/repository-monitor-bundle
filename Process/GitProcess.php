<?php
/**
 * \file GitProcess.php
 * \author Benoit Maziere <benoit.maziere@gmail.com> Pierre TRANCHARD <pierre@tranchard.net>
 * \version 1.0
 * \date 09/07/15
 * \brief
 * \details
 */

namespace Spark\RepositoryMonitorBundle\Process;

use Spark\RepositoryMonitorBundle\Exception\FolderNotFoundException;
use Symfony\Component\Process\Process;

/**
 * Class GitProcess
 *
 * @package Spark\RepositoryMonitorBundle\Process
 */
class GitProcess
{

    /**
     * @param string|null $output
     *
     * @return array
     */
    public static function convertOutputToArray($output = null)
    {
        if (is_null($output)) {
            $array = array();
        } else {
            $array = explode("\n", $output);
            array_pop($array);
        }

        return $array;
    }

    /**
     * @param string $projectDirectory
     * @param array  $errors
     * @param int    $commitPosition
     *
     * @return Process
     * @throws FolderNotFoundException
     */
    public function sha1Process($projectDirectory, array &$errors, $commitPosition = -1)
    {
        $gitDirectory = static::getDirectory($projectDirectory);
        $sha1Process  = new Process("git --git-dir=\"$gitDirectory\" log --format=%h | tail " . $commitPosition);

        $sha1Process->run(
            function ($type, $buffer) use (&$errors) {
                if ($type === Process::ERR) {
                    $errors[] = $buffer;
                }
            }
        );

        return $sha1Process;
    }

    /**
     * @param string $projectDirectory
     * @param array  $errors
     * @param bool   $tagOnly
     *
     * @return Process
     * @throws FolderNotFoundException
     */
    public function tagProcess($projectDirectory, array &$errors, $tagOnly = true)
    {
        $gitDirectory = static::getDirectory($projectDirectory);
        if ($tagOnly === true) {
            $commandLine = "git --git-dir=\"$gitDirectory\" tag | grep -v SNAPSHOT | grep -v RELEASE | xargs -I@ git --git-dir=\"$gitDirectory\" log --format=format:\"%ai @%n\" -1 @ | sort | awk '{print $4}'";
        } else {
            $commandLine = "git --git-dir=\"$gitDirectory\" tag | grep -v SNAPSHOT | grep -v RELEASE | xargs -I@ git --git-dir=\"$gitDirectory\" log --format=format:\"%ai @%n\" -1 @ | sort";
        }

        $gitTagsProcess = new Process($commandLine);
        $gitTagsProcess->run(
            function ($type, $buffer) use (&$errors) {
                if ($type === Process::ERR) {
                    $errors[] = $buffer;
                }
            }
        );

        return $gitTagsProcess;
    }

    /**
     * @param string $projectDirectory
     * @param array  $errors
     * @param null   $tag
     *
     * @return Process
     * @throws FolderNotFoundException
     */
    public function showRefsTags($projectDirectory, array &$errors, $tag = null)
    {
        $gitDirectory = static::getDirectory($projectDirectory);
        if (is_null($tag) === true) {
            $commandLine = sprintf("git --git-dir=\"%s\" show-ref --tags", $gitDirectory);
        } else {
            $commandLine = sprintf("git --git-dir=\"%s\" show-ref --tags %s", $gitDirectory, $tag);
        }

        $showRefsTagsProcess = new Process($commandLine);
        $showRefsTagsProcess->run(
            function ($type, $buffer) use (&$errors) {
                if ($type === Process::ERR) {
                    $errors[] = $buffer;
                }
            }
        );

        return $showRefsTagsProcess;
    }

    /**
     * @param string $projectDirectory
     * @param string $lowTagThreshold
     * @param string $highTagThreshold
     * @param array  $errors
     *
     * @return Process
     */
    public function logProcess($projectDirectory, $lowTagThreshold, $highTagThreshold, array &$errors)
    {
        $gitDirectory = static::getDirectory($projectDirectory);
        $commandLine  = sprintf(
            "git --git-dir=\"%s\" log --no-merges --date=short --pretty=format:\"%%h    %%x09%%ad     %%x09%%m    %%x09%%s\" %s..%s | grep -v 'Preparing merge of'",
            $gitDirectory,
            $lowTagThreshold,
            $highTagThreshold
        );

        $logsProcess = new Process($commandLine);
        $logsProcess->run(
            function ($type, $buffer) use (&$errors) {
                if ($type === Process::ERR) {
                    $errors[] = $buffer;
                }
            }
        );

        return $logsProcess;
    }

    /**
     * @param string $projectDirectory
     * @param string $grepParameters
     * @param array  $errors
     *
     * @return Process
     */
    public function grepProcess($projectDirectory, $grepParameters, array &$errors = array())
    {
        $gitDirectory   = static::getDirectory($projectDirectory);
        $grepProcess = new Process(
            sprintf(
                "git --git-dir=\"%s\" log --pretty=format:\"%C(yellow)%h|%Cred%ad|%Cblue%an|%Cgreen%d %Creset%s\" --date=short | less -r | grep %s",
                $gitDirectory,
                $grepParameters
            )
        );
        $grepProcess->run(
            function ($type, $buffer) use (&$errors) {
                if ($type === Process::ERR) {
                    $errors[] = $buffer;
                }
            }
        );

        return $grepProcess;
    }

    /**
     * @param $projectDirectory
     *
     * @return string
     * @throws FolderNotFoundException
     */
    protected static function getDirectory($projectDirectory)
    {
        if ($projectDirectory === false) {
            throw new FolderNotFoundException();
        }

        return $projectDirectory . DIRECTORY_SEPARATOR . ".git";
    }
}
