<?php
/**
 * \file RepositoryHandler.php
 * \author Pierre TRANCHARD <pierre@tranchard.net>
 * \version 1.0
 * \date 13/07/15
 * \brief
 * \details
 */

namespace Spark\RepositoryMonitorBundle\Form\Handler;

use Spark\FrameworkBundle\Form\Factory\FormFactory;
use Spark\RepositoryMonitorBundle\Entity\Repository;
use Spark\RepositoryMonitorBundle\Manager\RepositoryManager;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Class RepositoryHandler
 *
 * @package Spark\RepositoryMonitorBundle\Form\Handler
 */
class RepositoryHandler
{

    /**
     * @var RequestStack
     */
    protected $requestStack;

    /**
     * @var FormInterface
     */
    protected $form;

    /**
     * @var RepositoryManager
     */
    protected $manager;

    /**
     * Constructor
     *
     * @param RequestStack      $requestStack
     * @param FormFactory       $formFactory
     * @param RepositoryManager $manager
     */
    public function __construct(RequestStack $requestStack, FormFactory $formFactory, RepositoryManager $manager)
    {
        $this->requestStack = $requestStack;
        $this->form         = $formFactory->createForm();
        $this->manager      = $manager;
    }

    /**
     * @param Repository $repository
     *
     * @return bool
     */
    public function handleRequest(Repository $repository)
    {
        $this->form->setData($repository);
        $request = $this->requestStack->getCurrentRequest();
        if ($request->isMethod('POST') || $request->isXmlHttpRequest()) {
            $this->form->handleRequest($request);
            if ($this->form->isValid()) {
                return $this->onSuccess($repository);
            }
        }

        return false;
    }

    /**
     * @param Repository $repository
     *
     * @return bool
     */
    protected function onSuccess(Repository $repository)
    {
        try {
            $this->manager->persistInstance($repository);
            $this->manager->getEntityManager()->flush($repository);

            return true;
        } catch (\Exception $exception) {
            return false;
        }
    }

    /**
     * @return FormInterface
     */
    public function getForm()
    {
        return $this->form;
    }
}
