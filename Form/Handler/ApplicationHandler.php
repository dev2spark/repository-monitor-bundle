<?php
/**
 * \file ApplicationHandler.php
 * \author Pierre TRANCHARD <pierre@tranchard.net>
 * \version 1.0
 * \date 20/06/15
 * \brief
 * \details
 */

namespace Spark\RepositoryMonitorBundle\Form\Handler;

use Spark\FrameworkBundle\Form\Factory\FormFactory;
use Spark\RepositoryMonitorBundle\Entity\Application;
use Spark\RepositoryMonitorBundle\Manager\ApplicationManager;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Class ApplicationHandler
 *
 * @package Spark\RepositoryMonitorBundle\Form\Handler
 */
class ApplicationHandler
{

    /**
     * @var RequestStack
     */
    protected $requestStack;

    /**
     * @var FormInterface
     */
    protected $form;

    /**
     * @var ApplicationManager
     */
    protected $manager;

    /**
     * Constructor
     *
     * @param RequestStack       $requestStack
     * @param FormFactory        $formFactory
     * @param ApplicationManager $manager
     */
    public function __construct(RequestStack $requestStack, FormFactory $formFactory, ApplicationManager $manager)
    {
        $this->requestStack = $requestStack;
        $this->form         = $formFactory->createForm();
        $this->manager      = $manager;
    }

    /**
     * @param Application $application
     *
     * @return bool
     */
    public function handleRequest(Application $application)
    {
        $this->form->setData($application);
        $request = $this->requestStack->getCurrentRequest();
        if ($request->isMethod('POST') || $request->isXmlHttpRequest()) {
            $this->form->handleRequest($request);
            if ($this->form->isValid()) {
                return $this->onSuccess($application);
            }
        }

        return false;
    }

    /**
     * @param Application $application
     *
     * @return bool
     */
    protected function onSuccess(Application $application)
    {
        try {
            $this->manager->persistInstance($application);
            $this->manager->getEntityManager()->flush($application);

            return true;
        } catch (\Exception $exception) {
            return false;
        }
    }

    /**
     * @return FormInterface
     */
    public function getForm()
    {
        return $this->form;
    }
}
