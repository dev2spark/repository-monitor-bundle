<?php
/**
 * \file ClientHandler.php
 * \author Pierre TRANCHARD <pierre@tranchard.net>
 * \version 1.0
 * \date 26/06/15
 * \brief
 * \details
 */

namespace Spark\RepositoryMonitorBundle\Form\Handler;

use Spark\FrameworkBundle\Form\Factory\FormFactory;
use Spark\RepositoryMonitorBundle\Entity\Client;
use Spark\RepositoryMonitorBundle\Manager\ClientManager;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Class ClientHandler
 *
 * @package Spark\RepositoryMonitorBundle\Form\Handler
 */
class ClientHandler
{

    /**
     * @var RequestStack
     */
    protected $requestStack;

    /**
     * @var FormInterface
     */
    protected $form;

    /**
     * @var ClientManager
     */
    protected $manager;

    /**
     * Constructor
     *
     * @param RequestStack  $requestStack
     * @param FormFactory   $formFactory
     * @param ClientManager $manager
     */
    public function __construct(RequestStack $requestStack, FormFactory $formFactory, ClientManager $manager)
    {
        $this->requestStack = $requestStack;
        $this->form         = $formFactory->createForm();
        $this->manager      = $manager;
    }

    /**
     * @param Client $client
     *
     * @return bool
     */
    public function handleRequest(Client $client)
    {
        $this->form->setData($client);
        $request = $this->requestStack->getCurrentRequest();
        if ($request->isMethod('POST') || $request->isXmlHttpRequest()) {
            $this->form->handleRequest($request);
            if ($this->form->isValid()) {
                return $this->onSuccess($client);
            }
        }

        return false;
    }

    /**
     * @param Client $client
     *
     * @return bool
     */
    protected function onSuccess(Client $client)
    {
        try {
            $this->manager->persistInstance($client);
            $this->manager->getEntityManager()->flush($client);

            return true;
        } catch (\Exception $exception) {
            return false;
        }
    }

    /**
     * @return FormInterface
     */
    public function getForm()
    {
        return $this->form;
    }
}