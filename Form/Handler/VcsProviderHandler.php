<?php
/**
 * \file VcsProviderHandler.php
 * \author Pierre TRANCHARD <pierre@tranchard.net>
 * \version 1.0
 * \date 13/07/15
 * \brief
 * \details
 */

namespace Spark\RepositoryMonitorBundle\Form\Handler;

use Spark\FrameworkBundle\Form\Factory\FormFactory;
use Spark\RepositoryMonitorBundle\Entity\VcsProvider;
use Spark\RepositoryMonitorBundle\Manager\VcsProviderManager;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Class VcsProviderHandler
 *
 * @package Spark\RepositoryMonitorBundle\Form\Handler
 */
class VcsProviderHandler
{

    /**
     * @var RequestStack
     */
    protected $requestStack;

    /**
     * @var FormInterface
     */
    protected $form;

    /**
     * @var VcsProviderManager
     */
    protected $manager;

    /**
     * Constructor
     *
     * @param RequestStack       $requestStack
     * @param FormFactory        $formFactory
     * @param VcsProviderManager $manager
     */
    public function __construct(RequestStack $requestStack, FormFactory $formFactory, VcsProviderManager $manager)
    {
        $this->requestStack = $requestStack;
        $this->form         = $formFactory->createForm();
        $this->manager      = $manager;
    }

    /**
     * @param VcsProvider $vcsProvider
     *
     * @return bool
     */
    public function handleRequest(VcsProvider $vcsProvider)
    {
        $this->form->setData($vcsProvider);
        $request = $this->requestStack->getCurrentRequest();
        if ($request->isMethod('POST') || $request->isXmlHttpRequest()) {
            $this->form->handleRequest($request);
            if ($this->form->isValid()) {
                return $this->onSuccess($vcsProvider);
            }
        }

        return false;
    }

    /**
     * @param VcsProvider $vcsProvider
     *
     * @return bool
     */
    protected function onSuccess(VcsProvider $vcsProvider)
    {
        try {
            $this->manager->persistInstance($vcsProvider);
            $this->manager->getEntityManager()->flush($vcsProvider);

            return true;
        } catch (\Exception $exception) {
            return false;
        }
    }

    /**
     * @return FormInterface
     */
    public function getForm()
    {
        return $this->form;
    }
}
