<?php
/**
 * \file ApplicationType.php
 * \author Pierre TRANCHARD <pierre@tranchard.net>
 * \version 1.0
 * \date 20/06/15
 * \brief
 * \details
 */

namespace Spark\RepositoryMonitorBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class ApplicationType
 *
 * @package Spark\RepositoryMonitorBundle\Form\Type
 */
class ApplicationType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add(
            'client',
            'entity',
            array(
                'class'        => 'SparkRepositoryMonitorBundle:Client',
                'choice_label' => 'name',
            )
        );
        $builder->add(
            'repository',
            'entity',
            array(
                'class'        => 'SparkRepositoryMonitorBundle:Repository',
                'choice_label' => 'uri',
            )
        );
        $builder->add('name', 'text');
        $builder->add('directory', 'text');
        $builder->add('uri', 'url', array('required' => false));
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            array(
                'validation_groups' => array('application'),
                'data_class'        => 'Spark\RepositoryMonitorBundle\Entity\Application',
                'csrf_token_id'     => $this->getName(),
            )
        );
    }

    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return 'spark_repository_monitor_form_application';
    }
}
