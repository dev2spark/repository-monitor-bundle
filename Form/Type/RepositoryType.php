<?php
/**
 * \file RepositoryType.php
 * \author Pierre TRANCHARD <pierre@tranchard.net>
 * \version 1.0
 * \date 13/07/15
 * \brief
 * \details
 */

namespace Spark\RepositoryMonitorBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class RepositoryType
 *
 * @package Spark\RepositoryMonitorBundle\Form\Type
 */
class RepositoryType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('uri', 'text');
        $builder->add(
            'vcsProvider',
            'entity',
            array(
                'class'        => 'SparkRepositoryMonitorBundle:VcsProvider',
                'choice_label' => 'name'
            )
        );
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            array(
                'validation_groups' => array('repository'),
                'data_class'        => 'Spark\RepositoryMonitorBundle\Entity\Repository',
                'csrf_token_id'     => $this->getName(),
            )
        );
    }

    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return 'spark_repository_monitor_form_repository';
    }
}
