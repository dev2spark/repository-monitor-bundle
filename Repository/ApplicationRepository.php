<?php
/**
 * \file ApplicationRepository.php
 * \author Pierre TRANCHARD <pierre@tranchard.net>
 * \version 1.0
 * \date 20/06/15
 * \brief
 * \details
 */

namespace Spark\RepositoryMonitorBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * Class ApplicationRepository
 *
 * @package Spark\RepositoryMonitorBundle\Repository
 */
class ApplicationRepository extends EntityRepository
{

    /**
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function findAllQueryBuilder()
    {
        return $this->createQueryBuilder('applications');
    }
}
