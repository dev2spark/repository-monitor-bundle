<?php
/**
 * \file ClientRepository.php
 * \author Pierre TRANCHARD <pierre@tranchard.net>
 * \version 1.0
 * \date 22/06/15
 * \brief
 * \details
 */

namespace Spark\RepositoryMonitorBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * Class ClientRepository
 *
 * @package Spark\RepositoryMonitorBundle\Repository
 */
class ClientRepository extends EntityRepository
{

    /**
     * @return mixed
     */
    public function findAll()
    {
        $queryBuilder = $this->createQueryBuilder('clients');

        return $queryBuilder->getQuery()->execute();
    }

    /**
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function findAllQueryBuilder()
    {
        return $this->createQueryBuilder('clients');
    }
}
