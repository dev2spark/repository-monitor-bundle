<?php

namespace Spark\RepositoryMonitorBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Class SparkRepositoryMonitorBundle
 *
 * @package Spark\RepositoryMonitorBundle
 *
 * @codeCoverageIgnore
 */
class SparkRepositoryMonitorBundle extends Bundle
{
}
