<?php

namespace Spark\RepositoryMonitorBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\Extension\PrependExtensionInterface;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader;

/**
 * This is the class that loads and manages your bundle configuration
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html}
 *
 * @codeCoverageIgnore
 */
class SparkRepositoryMonitorExtension extends Extension implements PrependExtensionInterface
{

    /**
     * {@inheritdoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config        = $this->processConfiguration($configuration, $configs);

        static::addBundleConfigurationInContainer(Configuration::getRootNode(), $config, $container);
        static::loadXMLFiles(new Loader\XmlFileLoader($container, new FileLocator(__DIR__ . '/../Resources/config')));
        static::loadYAMLFiles(new Loader\YamlFileLoader($container, new FileLocator(__DIR__ . '/../Resources/config')));
    }

    /**
     * @param string           $rootNode
     * @param array            $config
     * @param ContainerBuilder $container
     */
    protected static function addBundleConfigurationInContainer($rootNode, array $config, ContainerBuilder $container)
    {
        foreach ($config as $key => $parameter) {
            $parameterKey = sprintf("%s.%s", $rootNode, $key);
            $container->setParameter($parameterKey, $parameter);
        }
    }

    /**
     * @param Loader\XmlFileLoader $loader
     */
    protected static function loadXMLFiles(Loader\XmlFileLoader $loader)
    {
        $loader->load('services.xml');
        $loader->load('managers.xml');
        $loader->load('forms.xml');
        $loader->load('controllers.xml');
        $loader->load('twig.xml');
    }

    /**
     * @param Loader\YamlFileLoader $loader
     */
    protected static function loadYAMLFiles(Loader\YamlFileLoader $loader)
    {
        $loader->load('parameters.yml');
    }

    /**
     * Allow an extension to prepend the extension configurations.
     *
     * @param ContainerBuilder $container
     */
    public function prepend(ContainerBuilder $container)
    {
        if ($container->hasExtension('old_sound_rabbit_mq')) {
            $bundles = $container->getParameter('kernel.bundles');
            if (isset($bundles['OldSoundRabbitMqBundle'])) {
                static::loadOldSoundRabbitMQConfiguration(
                    new Loader\YamlFileLoader($container, new FileLocator(__DIR__ . '/../Resources/config'))
                );
            }
            $loginPath = null;
            if (isset($bundles['SecurityBundle'])) {
                $securityConfiguration = $container->getExtensionConfig('security');
                $securityConfiguration = isset($securityConfiguration[0]) ? $securityConfiguration[0] : array();

                if (isset($securityConfiguration['firewalls']) && is_array($securityConfiguration['firewalls'])) {
                    if (count($securityConfiguration['firewalls'])) {
                        $providerName = key($securityConfiguration['firewalls']);
                        $loginPath    = $this->findLoginPath($securityConfiguration['firewalls'][$providerName]);
                    } else {
                        foreach (array_keys($securityConfiguration['firewalls']) as $providerName) {
                            $loginPath = $this->findLoginPath($securityConfiguration['firewalls'][$providerName]);
                            if (is_string($loginPath)) {
                                break;
                            }
                        }
                    }
                }
            }
            if (is_null($loginPath) === false) {
                $container->setParameter(sprintf('%s_login_path', Configuration::getRootNode()), $loginPath);
            }
        }
    }

    /**
     * @param array $configuration
     *
     * @return null|string
     */
    protected function findLoginPath(array $configuration)
    {
        if (isset($configuration['login_path'])) {
            return $configuration['login_path'];
        }
        foreach ($configuration as $key => $options) {
            if (is_array($options)) {
                return $this->findLoginPath($configuration[$key]);
            }
        }

        return null;
    }

    /**
     * @param Loader\YamlFileLoader $loader
     */
    protected static function loadOldSoundRabbitMQConfiguration(Loader\YamlFileLoader $loader)
    {
        $loader->load('rabbit_mq.yml');
    }
}
