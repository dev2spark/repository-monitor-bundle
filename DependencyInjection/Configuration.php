<?php

namespace Spark\RepositoryMonitorBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\ArrayNodeDefinition;
use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files
 *
 * To learn more see {@link
 * http://symfony.com/doc/current/cookbook/bundles/extension.html#cookbook-bundles-extension-config-class}
 *
 * @codeCoverageIgnore
 */
class Configuration implements ConfigurationInterface
{

    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode    = $treeBuilder->root(static::getRootNode());
        static::addBundleConfiguration($rootNode);

        return $treeBuilder;
    }

    /**
     * @param ArrayNodeDefinition $node
     *
     * @return ArrayNodeDefinition
     */
    protected static function addBundleConfiguration(ArrayNodeDefinition $node)
    {
        $node
            ->children()
                ->scalarNode('login_route')->isRequired()->end()
                ->scalarNode('logout_route')->isRequired()->end()
                ->scalarNode('ticket_manager_uri')->isRequired()->end()
                ->scalarNode('ticket_regex')->defaultValue("[a-zA-Z]+-\d+")->end()
                ->arrayNode('base_folders')->prototype('scalar')->end()->defaultValue(array('Sites', 'www'))->end()
                ->integerNode('default_shown_tags')->defaultValue(3)->end()
            ->end();

        return $node;
    }


    /**
     * Get Root Node.
     *
     * @return string
     */
    public static function getRootNode()
    {
        return 'spark_repository_monitor';
    }
}
